var stage;
var l;


// Background
var bg = new createjs.Shape();

// RESET Button
var resetButton = new createjs.Shape();
var resetText;

var resetIt = 0;
var resetIt2 = 0;

var timeIn;
var timeOut = 120 * 1000; // Sekunden * 1000

// Hauptvariable für die Modusauswahl/-wechsel
var modus = 0;

// Textgroesse der Arbeitsanweisungen in allen Spielmodi
var schriftGroesse = 24;

// Auswahlboxen für den Startscreen
var auswahlboxen = [];
var container = [0, 0, 0, 0, 0];

// Textbausteine für den Startscreen
var text1, text2, text3, text4;
var aufgabentext = [0, 0, 0, 0];


// Anzahl der Spieler für Splitscreenmethode
var spieleranzahl = 0;
var spielerText = new createjs.Text("", "14px Arial Black", "navy");


// Arrays zur getrennten Speicherung/Berechnung des Spielfelds
var MEMORY1 = [];
var MEMORY2 = [];
var MEMORY3 = [];
var MEMORY4 = [];

// Arrays für die weißen Felder (Einführungsmodus)
var WHITEBOX1 = [];
var WHITEBOX2 = [];
var WHITEBOX3 = [];
var WHITEBOX4 = [];

// Variablen für die Boxen/Farbzetteln
var box, boxwidth, boxheight;
var abstand;
var zettelFarben = ["#911322", "#00004c", "#FECA1E", "#DE7A11", "#993299", "#400040", "#006600", "#4ca64c"];
var shiftFarbe;
var randomBit; // Zufallsvariable: 0 oder 1 (farbeX oder farbeX+1)
var offsetX, offsetY;


// Fehlerspeicherung
var fehlerP1, fehlerP2, fehlerP3, fehlerP4;
fehlerP1 = fehlerP2 = fehlerP3 = fehlerP4 = 0;
var fehlerTut = [];


// Speicherung der fertigen Spieler
var alleFertig = 0;
var FERTIG = [0, 0, 0, 0];



// Ranking
var rankingCount = 0;
var RANK = [];

//Gametutorial
var RICHTIGZETTEL =[0,0,0,0];
var FALSCHZETTEL =[0,0,0,0];

function init() {
		resetText = new createjs.Text((l("%neustart")), "40px Helvetica", "white");

    // Hintergrund und RESET-Button initialisieren
    bg.graphics.beginLinearGradientFill(["#123456", "rgba(0, 0, 0, 0)"], [0, 1], 0, 0, stage.canvas.width, stage.canvas.height)
    bg.graphics.drawRect(0, 0, stage.canvas.width, stage.canvas.height);
    bg.addEventListener("pressup", function(event) {
        timeIn = (new Date()).getTime() + timeOut;
        resetIt = 0;
        resetIt2 = 0;
    });
    resetButton.x = (stage.canvas.width - 150) / 2;
    resetButton.y = (stage.canvas.height - 50) / 2;
    resetButton.graphics.setStrokeStyle(4).beginStroke('white').beginFill("#8b0000").drawRoundRect(0, 0, 170, 50, 6, 6);

    resetText.x = resetButton.x + 8;
    resetText.y = resetButton.y + 2;

    resetButton.addEventListener("pressup", function(event) {
        window.location.reload();
    });
    resetButton.visible = resetText.visible = false;

    start();
}

function handleTick(event) {
    inactivityCheck();
}

function inactivityCheck() {
    var currentTime = (new Date()).getTime();
    if ((currentTime > timeIn && timeIn != 0) || (resetIt == 1 && resetIt2 == 1)) {
        resetButton.visible = resetText.visible = true;
        stage.addChild(bg);
        stage.addChild(resetButton, resetText);
        bg.alpha = 0.9;
    } else {
        resetButton.visible = resetText.visible = false;
        stage.addChildAt(bg, 0);
        bg.alpha = 1;
    }

}

//Register Init function with the MTLG framework
MTLG.addGameInit((pOptions) => {
  //set globals
  stage = MTLG.gibStage();
	l = MTLG.lang.getString;
	init();
	// EventListener wird benoetigt, um die Aktualisierung zu jedem angegebenen Tick zu erzeugen.
	createjs.Ticker.addEventListener("tick", handleTick);
  MTLG.lc.registerMenu(start);
  // MTLG.lc.registerLevel(spiel_start, ()=>1);

});
