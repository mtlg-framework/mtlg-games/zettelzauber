MTLG.lang.define({
	"de":{
		//init.js
		//init()
		"%neustart" : "Neustart",
		//spiel.js
		//start()
		"%spielbeginnPlural" : "Lasst uns loslegen!",
		"%spielbeginnSingular" : "Lass uns loslegen!",
		"%spieler" : " Spieler",
		"%frageSpieleranzahl" : "Wie viele Spieler seid ihr?",
		//erstelleFeld()
		"%richtig" : "✓",
		"%euleRichtig" : "Sehr gut!",
		"%falsch" : "X",
		//eule()
		"%aufgabeModus0" : "Zähle die Farben!",
		"%aufgabeModus1" : "Ergänze, sodass die Anzahl der Zettel \nbei beiden Farben gerade ist!",
		"%aufgabeModus2" : "Ist die Anzahl der Zettel bei beiden \nFarben gerade oder ungerade?",
		"%aufgabeModus3" : "Suche den Fehler!",
		"%euleTippModus0" : "Tipp: Wenn eine Farbe nicht vorkommt, ist sie gerade.",
		"%euleTippModus1" : "Tipp: Mit dem Besen kannst du deine Zettel aufräumen.",
		"%euleTippModus2" : "Tipp: Es ist egal welche der beiden Farben du zählst.",
		"%euleTippModus3" : "Tipp: Suche die Fehler- zeile und Fehlerspalte.",
		"%euleFalsch" : "Der Zettel war es nicht. Achte auf die Zeilen und Spalten!",
		"%euleDreiFalsch" : "Du hast kein ♥ mehr verfügbar. Versuchs nochmal ;)",
		//ranking()
		"%erster" : "Erster : ✰✰✰✰",
		"%zweiter" : "Zweiter : ✰✰✰",
		"%dritter" : "Dritter : ✰✰",
		"%vierter" : "Vierter : ✰",
		"%aufgabeWarten" : "Warte bis alle fertig sind.",
		//modi.js
		//stapelZettel
		"%euleRichtigeFarbe" : "Das war die richtige Farbe!",
		"%euleFalscheFarbe" : "Das war nicht die richtige Farbe, versuchs nochmal!",
		//gameTutorial()
		"%euleRichtigeZeile" : "Sehr gut! Auf zur nächsten Zeile.",
		"%euleSpaltenStart" : "Betrachte nun die Spalten.",
		"%euleFalscheZeile" : "Versuchs nochmal!",
		"%euleFalscheSpalte" : "Zähle nochmal die Farben der Spalte!",
		"%euleRichtigeSpalte" : "Sehr gut! Auf zur nächsten Spalte.",
		"%euleTippErsteFehlersuche" : "Achte darauf, wo sich Fehler- zeile und -spalte treffen!",
		"%aufgabeErsteFehlersuche" : "Finde nun den Fehler!",
		"%euleRichtigWarten" : "Sehr gut! Warte nun auf deine Mitspieler...",
		//geradeUngerade()
		"%frageGeradeUngerade" : "Welche ist ",
		"%gerade" : "gerade?",
		"%ungerade" : "ungerade?",
		//endScreen()
		"%start" : "Start",
		"%fehlersuche" : "Fehlersuche",
		"%einfuehrung" : "Einführung",
		"%pruefungBestanden" : "Zauberprüfung bestanden!",
		"%ersterPlatz" : "Erster Platz",
		"%zweiterPlatz" : "Zweiter Platz",
		"%dritterPlatz" : "Dritter Platz",
		"%vierterPlatz" : "Vierter Platz",
		"%ersterPlatzSterne" : "✰✰✰✰",
		"%zweiterPlatzSterne" : "✰✰✰",
		"%dritterPlatzSterne" : "✰✰",
		"%vierterPlatzSterne" : "✰",
		"%glueckwunsch" : "Glückwunsch: "
	},
	"en":{
		//init.js
		//init()
		"%neustart" : "Restart",
		//spiel.js
		//start()
		"%spielbeginnPlural" : "Let's start!",
		"%spielbeginnSingular" : "Let's start!",
		"%spieler" : " Player",
		"%frageSpieleranzahl" : "How many players are you?",
		//erstelleFeld()
		"%richtig" : "✓",
		"%euleRichtig" : "Perfect!",
		"%falsch" : "X",
		//eule()
		"%aufgabeModus0" : "Count the colours!",
		"%aufgabeModus1" : "Complete to get even numbers of notes for both colours!",
		"%aufgabeModus2" : "Are the numbers of notes of both colours even or uneven?",
		"%aufgabeModus3" : "Search the mistake!",
		"%euleTippModus0" : "Hint: If a colour does not appear, it's even",
		"%euleTippModus1" : "Hint: You can use the broom to tidy up.",
		"%euleTippModus2" : "Hint: It doesn't matter which colour you count.",
		"%euleTippModus3" : "Hint: Search the row and the column, where you find a mistake.",
		"%euleFalsch" : "This note was wrong. Look at the rows and the columns :)",
		"%euleDreiFalsch" : "There is no ♥ anymore. Try it again ;)",
		//ranking()
		"%erster" : "First : ✰✰✰✰",
		"%zweiter" : "Second : ✰✰✰",
		"%dritter" : "Third : ✰✰",
		"%vierter" : "Fourth : ✰",
		"%aufgabeWarten" : "Wait, till everyone is done",
		//modi.js
		//stapelZettel
		"%euleRichtigeFarbe" : "Das war die richtige Farbe!",
		"%euleFalscheFarbe" : "Das war nicht die richtige Farbe, versuchs nochmal!",
		//gameTutorial()
		"%euleRichtigeZeile" : "Very good, this row was correct. Go to the next line",
		"%euleSpaltenStart" : "Now look at the columns.",
		"%euleFalscheZeile" : "Try it again!",
		"%euleFalscheSpalte" : "Count the colours of the column again!",
		"%euleRichtigeSpalte" : "Very good! Go to the next column.",
		"%euleTippErsteFehlersuche" : "Look at the note, where the row and the column with the mistake inside meet",
		"%aufgabeErsteFehlersuche" : "Find the mistake!",
		"%euleRichtigWarten" : "Very good! :). Wait for the other players...",
		//geradeUngerade()
		"%frageGeradeUngerade" : "Which is ",
		"%gerade" : "even?",
		"%ungerade" : "uneven?",
		//endScreen()
		"%start" : "Start",
		"%fehlersuche" : "Searching the mistake",
		"%einfuehrung" : "Introduction",
		"%pruefungBestanden" : "Magician exam passed! :)",
		"%ersterPlatz" : "First place",
		"%zweiterPlatz" : "Second place",
		"%dritterPlatz" : "Third place",
		"%vierterPlatz" : "Fourth place",
		"%ersterPlatzSterne" : "✰✰✰✰",
		"%zweiterPlatzSterne" : "✰✰✰",
		"%dritterPlatzSterne" : "✰✰",
		"%vierterPlatzSterne" : "✰",
		"%glueckwunsch" : "Congratulations: "
	},
	"en-us":{
		//init.js
		//init()
		"%neustart" : "Restart",
		//spiel.js
		//start()
		"%spielbeginnPlural" : "Let's start!",
		"%spielbeginnSingular" : "Let's start!",
		"%spieler" : " Player",
		"%frageSpieleranzahl" : "How many players are you?",
		//erstelleFeld()
		"%richtig" : "✓",
		"%euleRichtig" : "Perfect!",
		"%falsch" : "X",
		//eule()
		"%aufgabeModus0" : "Count the colours!",
		"%aufgabeModus1" : "Complete to get even numbers of notes for both colours!",
		"%aufgabeModus2" : "Are the numbers of notes of both colours even or uneven?",
		"%aufgabeModus3" : "Search the mistake!",
		"%euleTippModus0" : "Hint: If a colour does not appear, it's even",
		"%euleTippModus1" : "Hint: You can use the broom to tidy up.",
		"%euleTippModus2" : "Hint: It doesn't matter which colour you count.",
		"%euleTippModus3" : "Hint: Search the row and the column, where you find a mistake.",
		"%euleFalsch" : "This note was wrong. Look at the rows and the columns :)",
		"%euleDreiFalsch" : "There is no ♥ anymore. Try it again ;)",
		//ranking()
		"%erster" : "First : ✰✰✰✰",
		"%zweiter" : "Second : ✰✰✰",
		"%dritter" : "Third : ✰✰",
		"%vierter" : "Fourth : ✰",
		"%aufgabeWarten" : "Wait, till everyone is done",
		//modi.js
		//stapelZettel
		"%euleRichtigeFarbe" : "Das war die richtige Farbe!",
		"%euleFalscheFarbe" : "Das war nicht die richtige Farbe, versuchs nochmal!",
		//gameTutorial()
		"%euleRichtigeZeile" : "Very good, this row was correct. Go to the next line",
		"%euleSpaltenStart" : "Now look at the columns.",
		"%euleFalscheZeile" : "Try it again!",
		"%euleFalscheSpalte" : "Count the colours of the column again!",
		"%euleRichtigeSpalte" : "Very good! Go to the next column.",
		"%euleTippErsteFehlersuche" : "Look at the note, where the row \nand the column with the mistake inside meet",
		"%aufgabeErsteFehlersuche" : "Find the mistake!",
		"%euleRichtigWarten" : "Very good! :). Wait for the other players...",
		//geradeUngerade()
		"%frageGeradeUngerade" : "Which is ",
		"%gerade" : "even?",
		"%ungerade" : "uneven?",
		//endScreen()
		"%start" : "Start",
		"%fehlersuche" : "Searching the mistake",
		"%einfuehrung" : "Introduction",
		"%pruefungBestanden" : "Magician exam passed! :)",
		"%ersterPlatz" : "First place",
		"%zweiterPlatz" : "Second place",
		"%dritterPlatz" : "Third place",
		"%vierterPlatz" : "Fourth place",
		"%ersterPlatzSterne" : "✰✰✰✰",
		"%zweiterPlatzSterne" : "✰✰✰",
		"%dritterPlatzSterne" : "✰✰",
		"%vierterPlatzSterne" : "✰",
		"%glueckwunsch" : "Congratulations: "
	}
});
